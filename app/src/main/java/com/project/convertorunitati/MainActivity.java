package com.project.convertorunitati;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity {

    String [] units;
    int pos = 0;
    int lastValue = 0;

    public MainActivity() {

    }

    void showToast(CharSequence msg) {
        Toast.makeText(this, msg, 0).show();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        Spinner spin = (Spinner)this.findViewById(R.id.unit_spinner);
        Button btnConvert = (Button)this.findViewById(R.id.btnConvert);
        final EditText txtEntry = (EditText)this.findViewById(R.id.txtEntry);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.unit_arrays, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);

        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.this.pos = position;
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        btnConvert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (txtEntry.getText().toString().trim().length() > 0) {
                    String textValue = txtEntry.getText().toString();
                    MainActivity.this.lastValue = Integer.parseInt(textValue);
                    if (MainActivity.this.pos == 0) {
                        double km = (double)(MainActivity.this.lastValue / 1000);
                        MainActivity.this.showToast(MainActivity.this.lastValue + " m = " + km + " km(s)");
                    } else if (MainActivity.this.pos == 1) {
                        double m = (double)(MainActivity.this.lastValue * 1000);
                        MainActivity.this.showToast(MainActivity.this.lastValue + " km(s) = " + m + " m");
                    } else {
                        double cm = (double)(MainActivity.this.lastValue * 100);
                        MainActivity.this.showToast(MainActivity.this.lastValue + " m = " + cm + " cm(s)");
                    }
                } else {
                    MainActivity.this.showToast("Please Enter Value");
                }

            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.main, menu);
        return true;





    }
}
